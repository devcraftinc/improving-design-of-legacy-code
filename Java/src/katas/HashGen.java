// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package katas;

public class HashGen {
    public interface HashSource {
        Iterable<Byte> getHashBytes();
    }

    public interface CompoundHashable {
        Iterable<Object> getParts();
    }

    public int getHash(Object o) {
        long result = 0L;
        if (o instanceof byte[]) {
            byte[] bytes = (byte[]) o;
            for (byte b : bytes) {
                result += b;
                result *= 2;
                result %= Integer.MAX_VALUE;
            }
        } else if (o instanceof String) {
            String s = (String) o;
            for (char c : s.toCharArray()) {
                result += c;
                result *= 17;
                result %= Integer.MAX_VALUE;
            }
        } else if (o instanceof Iterable) {
            Iterable<Object> enumerable = (Iterable<Object>) o;
            for (Object item : enumerable) {
                if (item instanceof byte[]) {
                    byte[] bytesItem = (byte[]) item;
                    for (byte b : bytesItem) {
                        result += b;
                        result *= 3;
                        result %= Integer.MAX_VALUE;
                    }

                } else if (item instanceof HashSource) {
                    HashSource hs = (HashSource) item;
                    for (byte b : hs.getHashBytes()) {
                        result += b;
                        result *= 5;
                        result %= Integer.MAX_VALUE;
                    }

                } else if (item instanceof CompoundHashable) {
                    CompoundHashable compound = (CompoundHashable) item;
                    for (Object part : compound.getParts()) {
                        result += getHash(part);
                        result *= 7;
                        result %= Integer.MAX_VALUE;
                    }
                } else if (item instanceof String) {
                    String s = (String) item;
                    for (char c : s.toCharArray()) {
                        result += c;
                        result *= 19;
                        result %= Integer.MAX_VALUE;
                    }
                }
            }
        } else if (o instanceof CompoundHashable) {
            CompoundHashable compound = (CompoundHashable) o;
            for (Object part : compound.getParts()) {
                result += getHash(part);
                result *= 11;
                result %= Integer.MAX_VALUE;
            }
        } else if (o instanceof HashSource) {
            HashSource hs = (HashSource) o;
            for (byte b : hs.getHashBytes()) {
                result += b;
                result *= 13;
                result %= Integer.MAX_VALUE;
            }
        } else {
            result = o.hashCode();
        }

        return (int) (result % Integer.MAX_VALUE);
    }
}

