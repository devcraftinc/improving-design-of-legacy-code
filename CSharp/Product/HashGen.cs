﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System.Collections;
using System.Collections.Generic;

namespace Product
{
    public class HashGen
    {
        public interface HashSource
        {
            IEnumerable<byte> GetHashBytes();
        }

        public interface CompoundHashable
        {
            IEnumerable GetParts();
        }

        public int GetHash(object o)
        {
            var result = 0L;
            switch (o)
            {
                case byte[] bytes:
                    foreach (var b in bytes)
                    {
                        result += b;
                        result *= 2;
                        result %= int.MaxValue;
                    }

                    break;
                case string s:
                    foreach (var c in s)
                    {
                        result += c;
                        result *= 17;
                        result %= int.MaxValue;
                    }
                    break;
                case IEnumerable enumerable:
                    foreach (var item in enumerable)
                    {
                        switch (item)
                        {
                            case byte[] bytesItem:
                                foreach (var b in bytesItem)
                                {
                                    result += b;
                                    result *= 3;
                                    result %= int.MaxValue;
                                }

                                break;
                            case HashSource hs:
                                foreach (var b in hs.GetHashBytes())
                                {
                                    result += b;
                                    result *= 5;
                                    result %= int.MaxValue;
                                }

                                break;
                            case CompoundHashable compound:
                                foreach (var part in compound.GetParts())
                                {
                                    result += GetHash(part);
                                    result *= 7;
                                    result %= int.MaxValue;
                                }

                                break;
                            case string s:
                                foreach (var c in s)
                                {
                                    result += c;
                                    result *= 19;
                                    result %= int.MaxValue;
                                }
                                break;
                        }
                    }
                    break;
                case CompoundHashable compound:
                    foreach (var part in compound.GetParts())
                    {
                        result += GetHash(part);
                        result *= 11;
                        result %= int.MaxValue;
                    }
                    break;
                case HashSource hs:
                    foreach (var b in hs.GetHashBytes())
                    {
                        result += b;
                        result *= 13;
                        result %= int.MaxValue;
                    }

                    break;
                default:
                    result = o.GetHashCode();
                    break;
            }

            return (int)(result % int.MaxValue);
        }
    }
}
